<!DOCTYPE html>
<html>
    <head>
        <title>Redirigiendo...</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Compras, En linea">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../Css/registro.css">
    </head>
    <body>
        <h1>Redirigiendo...</h1>
        <h2>Espere un segundo</h2>
        <a href="../Administrador/casetas.php?id=<?php echo $_GET["id"] ?>">Si no es redirigido automaticamente haga clic aquí</a>
    </body>
</html>

<?php
    require_once "../../Controlador/controladorFormularios.php";

    $res = ControlarFormularios::crtlEliCaseta();
    
    if ($res){
        //echo '<div class="alert-success">El usuario ha sido registrado</div>';
        header("Location: ../Administrador/casetas.php?id=".$_GET["id"]);
    }
?>