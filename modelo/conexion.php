<?php
class Conexion{

	static public function conectar(){
		$pwd = "12345";
		$usr = "postgres";
		$bd = "viaticos";
		$servidor = "127.0.0.1";
		$puerto = "5432";

		try {
			$baseDeDatos = new PDO("pgsql:host=$servidor;port=$puerto;dbname=$bd", $usr, $pwd);
			$baseDeDatos -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $baseDeDatos;
		} catch (Exception $e) {
			echo "Ocurrio un error en la base de datos: " . $e -> getMessage();
		}

	}	
}