<?php
    require_once "../../Controlador/controladorFormularios.php";

    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tu Perfil</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Página de registro de compras del usuario">
        <meta name="keywords" content="Compras, En linea">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../CSS/sesion.css">
</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <a class="navbar-brand" href="inicio.php?id=<?php echo $_GET["id"]?>">PER DIEM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a href="#" class="nav-link">Ver tu perfil</a></a>
                    </li>
                    <li class="nav-item active">
                        <a href="trabajo.php?id=<?php echo $_GET["id"]?>" class="nav-link" >Ver trabajo actual</a>
                    </li>
                    <li class="nav-item active">
                        <a href="automovil.php?id=<?php echo $_GET["id"]?>"class="nav-link">Datos del automóvil</a>
                    </li>
                </ul>
            </div>
        </nav>

		<div class="modal-dialog text-center">    
    		<div class="modal-content">
    		    <div class="row">
    		        <div class="col-12">
                        <br>
    		        	<h1>Perfil del usuario</h1>
                        <h5 class="text-danger">
                            <small>
                                Para actualizar el resto de datos, porfavor contacte a un administrador
                            </small>
                        </h5>
                        <hr>
    		        </div>
    		        <div class="tab-content ml-5">
    		            <form method="POST" action="../Llamadas/ActualizarContra.php" class="form" id="registrationForm">

                            <input type='hidden' name='id' value="<?php echo $res['id'] ?>">
                            <label class="text-left">
                                <h3>Nombre: <?php echo $res['nombre']." ".$res['apellidos'] ?></h3>
                                <h3>Empleado desde: <?php echo $res["fecha_contratacion"] ?></h3>
                                <h3>CURP: <?php echo $res["curp"] ?></h3>
                            </label>
                            <hr>
                           	<div class="form-label-group">
                                <label for="contra">
                                    Ingresa tu nueva contraseña<br>
                                </label>
                            	<input type="password" class="form-control" name="password" id="password" placeholder="Ingresa tu Contraseña" required>
                            </div><br>
                            <div class="form-label-group">
                                <label for="oldContra">Confirma tu contraseña anterior para guardar cambios</label>
                            	<input type="password" class="form-control" name="oldContra" id="oldContra" required>
                            </div>
                            <hr>
    		                <div class="form-group">
    		                    <div class="col-xs-12">
    								<button class="btn btn-lg btn-success" type="submit">
    		                        	Guardar
    		                    	</button>
    		                	</div>
    		            	</div>
    		       		</form>
    		    	</div>
    	    	</div>
    		</div>
		</div>
	</body>
</html>