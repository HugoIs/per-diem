<?php

require_once "../../modelo/crud.php";


define ("regexNombre", "/\b([A-Z]{1}[a-z]{1,30}[- ]{0,1} | [A-Z]{1}[- \']{1}[A-Z]{0,1}[a-z]{1,30}[- ]{0,1} | [a-z]{1,2}[ -\']{1}[A-Z]{1}[a-z]{1,30}){2,5}/");

define ("regexPassword", "/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-])*(.[a-z]{2,4})$/");

define ("regexCurp", "/[A-Z]{4}\d{6}(H|M){1}[A-Z]{5}(A|Z|\d){1}\d{1}/");

class ControlarFormularios{

	/*---------------------REGISTRAR---------------------*/

	/*-----Auto-----*/
	static public function ctrlRegAuto(){
		if (isset($_POST["modelo"]) && isset($_POST["año"]) &&
			isset($_POST["empresa"]) && isset($_POST["color"])){

			$tabla = "autos";

			$datos = array (
				"modelo" => $_POST["modelo"],
				"año" => $_POST["año"],
				"empresa" => $_POST["empresa"],
				"color" => $_POST["color"]
			);

			$respuesta = Crud::registroAuto($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*-----Casetas-----*/
	static public function ctrlRegCaseta(){
		if (isset($_POST["nombre"]) && isset($_POST["costo"]) && isset($_POST["carril"])){

			$tabla = "casetas";

			$datos = array (
				"nombre" => $_POST["nombre"],
				"costo" => $_POST["costo"],
				"carril" => $_POST["carril"]
			);

			$respuesta = Crud::registroCaseta($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*-----Empleado-----*/
	static public function ctrlRegEmpleado(){
		if (
			isset($_POST["nombre"]) &&
			isset($_POST["apellido"]) &&
			isset($_POST["correo"]) &&
			isset($_POST["contra"]) &&
			isset($_POST["curp"]) &&
			isset($_POST["fecha"]) &&
			isset($_POST["genero"]) &&
			isset($_POST["confirmar"]) &&
			isset($_POST["estados"]) ){

			$tabla = "empleado";

			$datos = array(
				"nombre" => $_POST["nombre"],
				"apellidos" => $_POST["apellido"],
				"curp" => $_POST["curp"],
				"fecha_contratacion"=>date('Y-m-d'),
				"usuario" => $_POST["correo"],
				"password" => $_POST["contra"],
				"puesto"=>"empleado"
			);

			$respuesta = Crud::registroEmpleado($tabla, $datos);
			return true;
		}else{
			$respuesta = "error";
		}
	}

	/*-----Ruta-----*/
	static public function ctrlRegRuta(){
		if (isset($_POST["empleado"]) && isset($_POST["auto"]) && isset($_POST["tarjetas"])){

			$tabla = "ruta";

			$datos = array (
				"empleado" => $_POST["empleado"],
				"auto" => $_POST["auto"],
				"tarjetas" => $_POST["tarjetas"]
			);

			$respuesta = Crud::registroRuta($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*-----Rutas-Casetas-----*/
	static public function ctrlRegRutaCaseta(){
		if (isset($_POST["caseta"]) && isset($_POST["ruta"])){

			$tabla = "rutacasetas";

			$datos= array(
				"caseta" => $_POST["caseta"],
				"ruta" => $_POST["ruta"]
			);

			$respuesta = Crud::registroRutaCaseta($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*---------------------SELECCIONAR---------------------*/
	static public function ctrlSeleccionar($tabla){
		$respuesta = Crud::seleccionar($tabla);

		return $respuesta;
	}

	static public function porId($tabla, $id){
		$respuesta = Crud::seleccionarPorId($tabla, $id);

		return $respuesta;
	}

	static public function ctrlSeleccionarFiltro($tabla, $item, $valor){

		$respuesta = Crud::seleccionarCondicion($tabla, $item, $valor);

		return $respuesta;
	}

	static public function ctrlInicioSesion(){

		if (isset($_POST["usuario"]) && isset($_POST["password"])){
			$datos = array(
				"usuario" => $_POST["usuario"],
				"password" => $_POST["password"]
			);

			$respuesta = Crud::seleccionarUsuario($datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	static public function ctrlInicioSesionDatos(){

		if (isset($_POST["usuario"]) && isset($_POST["password"])){
			$datos = array(
				"usuario" => $_POST["usuario"],
				"password" => $_POST["password"]
			);

			$respuesta = Crud::selUsuarioDatos($datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*---------------------ACTUALIZAR---------------------*/
	/*-----Auto-----*/
	static public function ctrlActAuto(){
		if (isset($_POST["modelo"]) && isset($_POST["año"]) &&
			isset($_POST["empresa"]) && isset($_POST["color"])){

			$tabla = "autos";
			$datos = array(
				"modelo" => $_POST["modelo"],
				"año" => $_POST["año"],
				"empresa" => $_POST["empresa"],
				"color" => $_POST["color"],
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarAuto($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*-----Caseta-----*/
	static public function ctrlActCaseta(){
		if (isset($_POST["nombre"]) && isset($_POST["costo"]) && isset($_POST["carril"])){

			$tabla = "casetas";

			$datos = array(
				"nombre" => $_POST["nombre"],
				"costo" => $_POST["costo"],
				"carril" => $_POST["carril"],
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarCaseta($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*-----Empleado-----*/
	static public function ctrlActEmpleado(){

		if (isset($_POST["nombre"]) &&
			isset($_POST["apellidos"]) &&
			isset($_POST["usuario"]) &&
			isset($_POST["password"]) &&
			isset($_POST["puesto"]) ){

			$tabla = "empleado";

			$datos = array (
				"nombre" => $_POST["nombre"],
				"apellidos" => $_POST["apellidos"],
				"usuario" => $_POST["usuario"],
				"password" => $_POST["password"],
				"puesto"=>$_POST["puesto"],
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarEmpleado($tabla, $datos);

			return $respuesta;	
		}else{
			$respuesta = "error";
		}	
	}

	static public function ctrlActContra(){
		if (isset($_POST["password"]) &&
			isset($_POST["oldContra"]) &&
			isset($_POST["id"]) ){

			$datos = array (
				"password" => $_POST["password"],
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarContra($datos);

			return $respuesta;	
		}else{
			$respuesta = "error";
		}
	}

	/*-----Ruta-----*/
	static public function ctrlActRuta(){
		if (isset($_POST["empleado"]) && isset($_POST["auto"]) &&isset($_POST["tarjetas"])){

			$tabla = "ruta";

			$datos = array(
				"empleado" => $_POST["empleado"],
				"auto" => $_POST["auto"],
				"tarjetas" => $_POST["tarjetas"], 
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarRuta($tabla, $datos);
		}else{
			$respuesta = "error";
		}
		return $respuesta;
	}

	/*-----Rutas-Casetas-----*/
	static public function ctrlActRutaCaseta(){
		if (isset($_POST["caseta"]) && isset($_POST["ruta"])){

			$tabla = "rutacasetas";

			$datos= array(
				"caseta" => $_POST["caseta"],
				"ruta" => $_POST["ruta"],
				"id"=>$_POST["id"]
			);

			$respuesta = Crud::actualizarRutaCaseta($tabla, $datos);
		}else{
			$respuesta = "error";
		}

		return $respuesta;
	}

	/*
	Por ahora se deja así separado, para no contener el nombre de la tabla en la página
	*/
	/*---------------------ELIMINAR---------------------*/
	/*-----Auto-----*/
	public function crtlEliAuto(){
		if (isset($_POST["id"])){
			$tabla = "autos";
			$datos = $_POST["id"];

			$respuesta = Crud::eliminar($tabla, $datos);
			return $respuesta;
		}
	}

	/*-----Casetas-----*/
	public function crtlEliCaseta(){
		if (isset($_POST["id"])){
			$tabla = "casetas";
			$datos = $_POST["id"];

			$respuesta = Crud::eliminar($tabla, $datos);
			return $respuesta;
		}
	}

	/*-----Empleados-----*/
	public function crtlEliEmpleado(){
		if (isset($_POST["id"])){
			$tabla = "empleado";
			$datos = $_POST["id"];

			$respuesta = Crud::eliminar($tabla, $datos);
			return $respuesta;
		}
	}

	/*-----Ruta-----*/
	public function crtlEliRuta(){
		if (isset($_POST["id"])){
			$tabla = "ruta";
			$datos = $_POST["id"];

			$respuesta = Crud::eliminar($tabla, $datos);
			return $respuesta;
		}
	}

	/*-----Rutas-Casetas-----*/
	public function crtlEliRutaCaseta(){
		if (isset($_POST["id"])){
			$tabla = "rutacasetas";
			$datos = $_POST["id"];

			$respuesta = Crud::eliminar($tabla, $datos);
			return $respuesta;
		}
	}
}