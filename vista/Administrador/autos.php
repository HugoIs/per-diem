<?php
    require_once "../../Controlador/controladorFormularios.php";

    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
    }

    $cars = ControlarFormularios::ctrlSeleccionar("autos");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administra Autos | PerDiem</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Página de administración de autos de PerDiem">
        <meta name="keywords" content="viaticos, administracion">
        <!--Librerías para bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        
        <script src="../JavaScript/validaciones.js"></script>
        <link rel="stylesheet" type="text/css" href="../CSS/registro.css">
    </head>
    <body>
        <div class="container-fluid bg-light">
            <div class="container">
                <ul class="nav nav-justified py-2 nav-pills">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Administrar autos</a> <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="empleados.php?id=<?php echo $_GET["id"] ?>" class="nav-link" >Administrar empleados</a>
                    </li>
                    <li class="nav-item">
                        <a href="casetas.php?id=<?php echo $_GET["id"] ?>" class="nav-link">Administrar casetas</a>
                    </li>
                    <li class="nav-item">
                        <a href="../sesion.html" class="nav-link">Salir</a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="text-center">
            <div class="">
                <div class="container-fluid">
                    <div class="col-12">
                        <h2>Administra automoviles</h2>
                    </div>
                    <div class="container py-2">
                        <table class="table table-bordered table-striped table-dark">
                            <thead>
                                <tr>
                                    <th>Modelo</th>
                                    <th>Año</th>
                                    <th>Empresa</th>
                                    <th>Color</th>
                                    <th colspan="2">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <form method="POST" action="../Llamadas/CrearAutos.php?id=<?php echo $_GET["id"] ?>">
                                	<tr>
                                        <td>
                                        	<input type="text" placeholder="Modelo" name="modelo" class="form-control">
                                        </td>
                                        <td>
                                        	<input type="text" placeholder="Año" name="año" class="form-control">
                                        </td>
                                        <td>
                                        	<input type="text" placeholder="Empresa" name="empresa" class="form-control">
                                        </td>
                                        <td>
                                        	<input type="text" placeholder="Color" name="color" class="form-control">
                                        </td>
                                        <td colspan="2">
                                        	<input type="submit" name="editar" value="Insertar" class="btn btn-success btn-block"> 
                                        </td>
                                    </tr>
                                </form>
                            <?php

                                foreach ($cars as $doc => $valor){
                                    echo
                                    "<tr>
                                        <form method='POST' action='../Llamadas/ActualizarAuto.php?id=".$_GET["id"]."'>
                                            <input type='hidden' name='id' value='".$valor["id"]."'>
                                            <td>
                                                <input type='text' name='modelo' value=".$valor["modelo"]."  class='form-control'>
                                            </td>
                                            <td>
                                                <input type='text' name='año' value='".$valor["año"]."' class='form-control'>
                                            </td>
                                            <td>
                                                <input type='text' name='empresa' value='".$valor["empresa"]."' class='form-control'>
                                            </td>
                                            <td>
                                                <input type='text' name='color' value='".$valor["color"]."' class='form-control'>
                                            </td>
                                            <td>
                                                <input type='submit' name='editar' value='Modificar' class='btn btn-primary'> 
                                            </td>
                                        </form>
                                        <form method='POST' action='../Llamadas/EliminarAuto.php?id=".$_GET["id"]."'>
                                            <input type='hidden' name='id' value='".$valor["id"]."'>
                                            <td>
                                                <input type='submit' name='eliminar' value='Eliminar' class='btn btn-danger'>
                                            </td>
                                        </form>
                                    </tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>       
    </body>
</html>