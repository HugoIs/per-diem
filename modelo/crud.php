<?php
//incluir la conexión
require_once "conexion.php";

class Crud{
	/*---------------------REGISTRAR---------------------*/

	/*-----Autos-----*/
	static public function registroAuto($tabla, $datos){

		$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla (modelo, año, empresa, color) VALUES (:modelo, :year, :empresa, :color)");

		$year = (int) $datos["año"];

		$stmt -> bindParam(":modelo",$datos["modelo"], PDO::PARAM_STR);
		$stmt -> bindParam(":year", $year, PDO::PARAM_INT);
		$stmt -> bindParam(":empresa",$datos["empresa"], PDO::PARAM_STR);
		$stmt -> bindParam(":color",$datos["color"], PDO::PARAM_STR);

		if ($stmt -> execute()){
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;
	}

	/*-----Casetas-----*/
	static public function registroCaseta($tabla, $datos){

		$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla (nombre, costo, carril) VALUES (:nombre, :costo, :carril)");

		$stmt -> bindParam(":nombre",$datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":costo", $datos["costo"], doubleval(PDO::PARAM_STR));
		$stmt -> bindParam(":carril",$datos["carril"], PDO::PARAM_STR);

		if ($stmt -> execute()){
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;
	}

	/*-----Empleado-----*/
	static public function registroEmpleado($tabla, $datos){

		$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla (auto, nombre, apellidos, curp, puesto, fecha_contratacion, usuario, password) VALUES (:auto, :nombre, :apellidos, :curp, :puesto, :fecha_contratacion, :usuario, :password)");

		$stmt -> bindParam(":auto",$datos["auto"], PDO::PARAM_INT);
		$stmt -> bindParam(":nombre",$datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellidos",$datos["apellidos"], PDO::PARAM_STR);
		$stmt -> bindParam(":curp",$datos["curp"], PDO::PARAM_STR);
		$stmt -> bindParam(":puesto",$datos["puesto"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha_contratacion",$datos["fecha_contratacion"], PDO::PARAM_STR);
		$stmt -> bindParam(":usuario",$datos["usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":password",$datos["password"], PDO::PARAM_STR);

		if ($stmt -> execute()){
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;
	}

	/*-----Ruta-----*/
	static public function registroRuta($tabla, $datos){

		$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla (empleado, auto, tarjetas) VALUES (:empleado, :auto, :tarjetas)");

		$emp = (int) $datos["empleado"];
		$aut = (int) $datos["auto"];
		$tar = (int) $datos["tarjetas"];

		$stmt -> bindParam(":empleado", $emp, PDO::PARAM_INT);
		$stmt -> bindParam(":auto", $aut, PDO::PARAM_INT);
		$stmt -> bindParam(":tarjetas", $tar, PDO::PARAM_INT);


		if ($stmt -> execute()){
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;
	}

	/*-----RutaCasetas-----*/
	static public function registroRutaCaseta($tabla, $datos){

		$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla (caseta, ruta) VALUES (:caseta, :ruta)");

		$cas = (int) $datos["caseta"];
		$rut = (int) $datos["ruta"];

		$stmt -> bindParam(":caseta", $cas, PDO::PARAM_INT);
		$stmt -> bindParam(":ruta", $rut, PDO::PARAM_INT);

		if ($stmt -> execute()){
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;
	}

	/*---------------------SELECCIONAR---------------------*/
	static public function seleccionar($tabla){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();  //devuelve todos los registros

	$stmt -> close();
	$stmt = null;

	}

	static public function seleccionarPorId($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id = :id ORDER BY id DESC");

		/*
			En la conexion ":item" se le asigna el valor de "valor"
			En caso de que no haya nada simplemente se ejecuta como un select normal
		*/
		$ref = (int) $id;
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch(); //Devuelve el primer registro que coincida con el valor		
	$stmt -> close();
	$stmt = null;	

	}


	static public function seleccionarCondicion($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :".$item ."ORDER BY id DESC");

		/*
			En la conexion ":item" se le asigna el valor de "valor"
			En caso de que no haya nada simplemente se ejecuta como un select normal
		*/
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch(); //Devuelve el primer registro que coincida con el valor		
	$stmt -> close();
	$stmt = null;		
	}

	static public function seleccionarUsuario($datos){
		$stmt = Conexion::conectar() -> prepare("SELECT * FROM empleado WHERE usuario = :usuario AND password =  :password");

		$stmt -> bindParam(":usuario",$datos["usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":password",$datos["password"], PDO::PARAM_STR);

		if ($stmt -> execute()){
			return true;
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;

	}

	static public function selUsuarioDatos($datos){
		$stmt = Conexion::conectar() -> prepare("SELECT * FROM empleado WHERE usuario = :usuario AND password =  :password");

		$stmt -> bindParam(":usuario",$datos["usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":password",$datos["password"], PDO::PARAM_STR);

		$stmt -> execute();


		return $stmt -> fetch();
		//Cerrar la conexion
		$stmt->close();
		$stmt=null;

	}

	/*---------------------ACTUALIZAR---------------------*/

	/*-----Autos-----*/
	static public function actualizarAuto($tabla,$datos){
		//modelo, año, empresa, color
		$stmt = Conexion::conectar() -> prepare ("UPDATE $tabla SET modelo=:modelo, año=:year, empresa=:empresa, color=:color WHERE id=:id");

		$ref = (int) $datos["id"];
		$year = (int) $datos["año"];

		$stmt->bindParam(":modelo", $datos["modelo"], PDO::PARAM_STR);
		$stmt->bindParam(":year", $year, PDO::PARAM_INT);
		$stmt->bindParam(":empresa", $datos["empresa"], PDO::PARAM_STR);
		$stmt->bindParam(":color", $datos["color"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}

	/*-----Casetas-----*/
	static public function actualizarCaseta($tabla,$datos){
		//nombre, costo, carril
		$stmt = Conexion::conectar() -> prepare ("UPDATE $tabla SET nombre=:nombre, costo=:costo, carril=:carril WHERE id=:id");

		$ref = (int) $datos["id"];

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":costo", $datos["costo"], doubleval(PDO::PARAM_STR));
		$stmt->bindParam(":carril", $datos["carril"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}

	/*-----Empleado-----*/
	static public function actualizarEmpleado($tabla,$datos){
		//auto, tarjeta, nombre, apellidos, curp, puesto, fecha_contratacion
		$stmt = Conexion::conectar() -> prepare ("UPDATE $tabla SET nombre=:nombre, apellidos=:apellidos, puesto=:puesto, usuario=:usuario, password=:password WHERE id=:id");

		$ref = (int) $datos["id"];

		$stmt -> bindParam(":nombre",$datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellidos",$datos["apellidos"], PDO::PARAM_STR);
		$stmt -> bindParam(":usuario",$datos["usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":password",$datos["password"], PDO::PARAM_STR);
		$stmt -> bindParam(":puesto",$datos["puesto"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return true;
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}

	static public function actualizarContra($datos){
		//auto, tarjeta, nombre, apellidos, curp, puesto, fecha_contratacion
		$stmt = Conexion::conectar() -> prepare ("UPDATE empleado SET password=:password WHERE id=:id");

		$ref = (int) $datos["id"];

		$stmt -> bindParam(":password",$datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return true;
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}

	/*-----Ruta-----*/
	static public function actualizarRuta($tabla,$datos){
		//empleado, auto, tarjetas
		$stmt = Conexion::conectar() -> prepare ("UPDATE $tabla SET empleado=:empleado, auto=:auto, tarjetas=:tarjetas WHERE id=:id");

		$ref = (int) $datos["id"];
		$emp = (int) $datos["empleado"];
		$aut = (int) $datos["auto"];
		$tar = (int) $datos["tarjetas"];

		$stmt -> bindParam(":empleado", $emp, PDO::PARAM_INT);
		$stmt -> bindParam(":auto", $aut, PDO::PARAM_INT);
		$stmt -> bindParam(":tarjetas", $tar, PDO::PARAM_INT);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}

	/*-----RutaCaseta-----*/
	static public function actualizarRutaCaseta($tabla,$datos){
		//caseta, ruta
		$stmt = Conexion::conectar() -> prepare ("UPDATE $tabla SET caseta=:caseta, ruta=:ruta WHERE id=:id");

		$ref = (int) $datos["id"];

		$stmt -> bindParam(":caseta",$datos["caseta"], PDO::PARAM_INT);
		$stmt -> bindParam(":ruta",$datos["ruta"], PDO::PARAM_INT);
		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		} else {
			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();
		$stmt = null;
	}


	/*---------------------ELIMINAR---------------------*/

	static public function eliminar($tabla, $valor){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id=:id");

		$ref = (int) $valor;

		$stmt->bindParam(":id", $ref, PDO::PARAM_INT);

		if ($stmt->execute()) {
			return true;
		} else {
			print_r(Conexion::conectar()->errroInfo());
		}
		$stmt -> close();
		$stmt = null;
	}
}
