<?php
    require_once "../../Controlador/controladorFormularios.php";

    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
        $car = ControlarFormularios::porId("autos", $res["auto"]);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Datos del automóvil | PerDiem</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Información del automóvil del usuario">
        <meta name="keywords" content="viaticos, administracion">
        <!--Librerías para bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        
        <script src="../JavaScript/validaciones.js"></script>
        <link rel="stylesheet" href="../CSS/estilos.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <a class="navbar-brand" href="inicio.php?id=<?php echo $_GET["id"]?>">PER DIEM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a href="perfil.php?id=<?php echo $_GET["id"]?>" class="nav-link">Ver tu perfil</a>
                    </li>
                    <li class="nav-item active">
                        <a href="trabajo.php?id=<?php echo $_GET["id"]?>" class="nav-link" >Ver trabajo actual</a>
                    </li>
                    <li class="nav-item active">
                        <a href="#"class="nav-link">Datos del automóvil</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="modal-dialog text-center">    
            <div class="modal-content">
                <div class="row">
                    <div class="col-12">
                        <br>
                        <h1>Datos del automovil</h1>
                        <hr>
                    </div>
                    <div class="tab-content ml-5">
                        <label class="text-left">
                            <h3>Modelo: <?php echo $car['modelo']." ".$car['año'] ?></h3>
                            <h3>Empresa desde: <?php echo $car["empresa"] ?></h3>
                            <h3>Color del automovil: <?php echo $car["color"] ?></h3>
                        </label>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
