<?php
    require_once "../../Controlador/controladorFormularios.php";

    $cars = ControlarFormularios::ctrlSeleccionar("empleado");
    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administra Empleados | PerDiem</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Página de administración de autos de PerDiem">
        <meta name="keywords" content="viaticos, administracion">
        <!--Librerías para bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        
        <script src="../JavaScript/validaciones.js"></script>
        <link rel="stylesheet" type="text/css" href="../CSS/registro.css">
    </head>
    <body>
        <div class="container-fluid bg-light">
            <div class="container">
                <ul class="nav nav-justified py-2 nav-pills">
                    <li class="nav-item">
                        <a href="autos.php?id=<?php echo $_GET["id"] ?>" class="nav-link">Administrar autos</a> <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" >Administrar empleados</a>
                    </li>
                    <li class="nav-item">
                        <a href="casetas.php?id=<?php echo $_GET["id"] ?>" class="nav-link">Administrar casetas</a>
                    </li>
                    <li class="nav-item">
                        <a href="../sesion.html" class="nav-link">Salir</a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="text-center">
            <div class="">
                <div class="container-fluid">
                    <div class="col-12">
                        <h2>Administra empleados</h2>
                    </div>
                    <div class="container py-2">
                        <table class="table table-bordered table-striped table-dark">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Contra.</th>
                                    <th>Puesto
                                    <th>Correo</th>
                                    <th>Contraseña</th>
                                    <th colspan="2">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>                                    
                                </tr>
                            <?php

                                foreach ($cars as $doc => $valor){
                                    echo
                                    "<tr>
                                        <form method='POST' action='../Llamadas/ActualizarEmpleado.php?id=".$_GET["id"]."'>
                                            <input type='hidden' name='id' value='".$valor["id"]."'>
                                            <td>
                                                <input type='text' name='nombre' value=".$valor["nombre"]."  class='form-control'>
                                            </td>
                                            <td>
                                                <input type='text' name='apellidos' value='".$valor["apellidos"]."' class='form-control'>
                                            </td>
                                            <td>
                                                ".$valor["fecha_contratacion"]."
                                            </td>
                                            <td>
                                                <select name='puesto' class='form-control'>
                                                    <option value='".$valor["puesto"]."' selected>";
                                                    if ($valor["puesto"] == "Soy Admin"){
                                                        echo "Administrador";
                                                    }else{
                                                        echo $valor["puesto"];
                                                    }
                                                    echo "</option>
                                                    <option value='empleado'>
                                                        Empleado
                                                    </option>
                                                    <option value='jefe'>
                                                        Jefe
                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type='text' name='usuario' value='".$valor["usuario"]."' class='form-control'>
                                            </td>
                                            <td>
                                                <input type='password' name='password' value='".$valor["password"]."' class='form-control'>
                                            </td>
                                            <td>
                                                <input type='submit' name='editar' value='Modificar' class='btn btn-primary'> 
                                            </td>
                                        </form>
                                        <form method='POST' action='../Llamadas/EliminarEmpleado.php?id=".$_GET["id"]."'>
                                            <input type='hidden' name='id' value='".$valor["id"]."'>
                                            <td>
                                                <input type='submit' name='eliminar' value='Eliminar' class='btn btn-danger'>
                                            </td>
                                        </form>
                                    </tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>       
    </body>
</html>