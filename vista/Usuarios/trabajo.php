<?php
    require_once "../../Controlador/controladorFormularios.php";

    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
        $car = ControlarFormularios::porId("autos", $res["auto"]);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Trabajo actual | PerDiem</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Información sobre el trabajo actual del usuario">
        <meta name="keywords" content="viaticos, administracion">
        <!--Librerías para bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

        <script src="../JavaScript/validaciones.js"></script>
        <link rel="stylesheet" href="../CSS/sesion.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <a class="navbar-brand" href="inicio.php?id=<?php echo $_GET["id"]?>">PER DIEM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a href="perfil.php?id=<?php echo $_GET["id"]?>" class="nav-link">Ver tu perfil</a>
                    </li>
                    <li class="nav-item active">
                        <a href="trabajo.php?id=<?php echo $_GET["id"]?>" class="nav-link" >Ver trabajo actual</a>
                    </li>
                    <li class="nav-item active">
                        <a href="automovil.php?id=<?php echo $_GET["id"]?>"class="nav-link">Datos del automóvil</a>
                    </li>
                </ul>
            </div>
        </nav>
<!--INFORMACIÓN GENERAAAAAAAAAAAAAAAAL-->
        <div class="modal-dialog text-center">
                <div class="modal-content">
                        <div class="form-group">
                            <h2>Registro</h2>
                        </div>
                        <div class="form-group text-left">
                            <h3>Bienvenido: <?php echo $res['nombre']." ".$res['apellidos'] ?></h3>
                        </div>
                        <div class="form-group text-left">
                            <h3>
                                Auto asignado:
                                <h5>Modelo: <?php echo $car['modelo']." ".$car['año'] ?></h5>
                                <h5>Empresa desde: <?php echo $car["empresa"] ?></h5>
                                <h5>Color del automovil: <?php echo $car["color"] ?></h5>
                            </h3>
                        </div>
                        <div class="form-group text-left">
                            <h3>Número de tarjeta de usuario: <?php echo $res['tarjeta'] ?></h3>
                        </div>
        
                        <div class="form-group text-left">
                            <h3>Total estimado del viaje es: $205.00</h3>
                        </div>
                </div>
        </div>
            
<!--TABLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA-->
        <div class="text-center">
            <div class="">
                <div class="container-fluid">
                    <div class="col-12">
                        <h2>Pagos en las casetas</h2>
                    </div>
                    <div class="container py-2">
                        <table class="table table-bordered table-striped table-dark">
                            <thead>
                                <tr>
                                    <th colspan="2">Caseta</th>
                                    <th>Estimación del sistema</th>
                                    <th>Pago real</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Camargo - Delicias</td>
                                    <td>Altavista</td>
                                    <td>$142</td>
                                    <td>$142</td>
                                </tr>
                                <tr>
                                    <td>Cardel - Veracruz</td>
                                    <td>La Antigua</td>
                                    <td>$63</td>
                                    <td>$ ...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
