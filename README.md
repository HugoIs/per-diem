# PER DIEM Leeme | Readme #

## Español ##

### Descripción del proyecto ###

El proyecto es una aplicación web desarrollada con el fin de administrar los viaticos utilizados por los empleados de una empresa para pagar las casetas. Esto se consigue utilizando el Internet de las Cosas (IoT por sus siglas en ingles) para enviar la información de los pagos a una base de datos que la aplicación web administra.

### ¿Para qué es este repositorio? ###

El repositorio es principalmente usado para almacenar los progresos realizados en el proyecto de la materia de Integradora II por los alumnos

### ¿Qué hace la aplicación? ###
La aplicación web podrá:
* Calcula los gastos estimados de un viaje basado en la información de las casetas del Estado de México.
* Administra los usuarios (empleados) de la empresa, así como los autos que se le asignan a cada uno.
* Presenta variados reportes que contengan información relevante, como reportes del último trabajo realizado o los gastos en todo el mes por ejemplo.

### Autores y agradecimientos ###
**Autores:**
* Sosa Alcauter Erik Emmanuel
* Trejo Castillo Uriel
* Vázquez Gutiérrez Hugo Isaac

**Agradecimientos:**
* Lic. Patricia Tenorio Soto - por ayudar con la documentación del proyecto
* Ing. María Luisa Morales Monroy - Por apoyar durante todo el proceso del proyecto

### ¿Con quién me puedo comunicar? ###

* Administrador del repositorio: Vázquez Gutiérrez Hugo Isaac | hivgy@outlook.com | hugoisaas@gmail.com
* Otros miembros de contacto: Trejo Castillo Uriel | 2519260018.utrejoc@gmail.com

## English ##

### Project Description ###

The project is a web application with the final purpose of manage the travel expenses made by a company's employees to pay tool booths. This is made possible by the use of Internet of Things (IoT) to send information of the payments to a databse that the web application manages.

### What is this repository for? ###
This repository is mainly used to save the progress made in the Integradora II subject project by the students

### What does the app do? ###
The web application will:

* Calculate the travel expenses based on the information from the tool booths in the Estate of Mexico.
* Manages the users (employees) of the company, as well as the cars asigned to them.
* Presents several reports that have relevant information, such as reports of the last job done or the expenses on the whole month.

### Authors and special thanks ###
**Authors:**
* Sosa Alcauter Erik Emmanuel
* Trejo Castillo Uriel
* Vázquez Gutiérrez Hugo Isaac

**Special Thanks to:**
* Lic. Patricia Tenorio Soto - for helping with the documentation of the project
* Ing. María Luisa Morales Monroy - For supporting during all the process of developing the project

### Who do I talk to? ###

* Repository admin: Vázquez Gutiérrez Hugo Isaac | hivgy@outlook.com
* Other members: Trejo Castillo Uriel | 2519260018.utrejoc@gmail.com