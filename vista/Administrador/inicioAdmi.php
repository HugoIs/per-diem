<?php
    require_once "../../Controlador/controladorFormularios.php";

    if (!isset($_GET["id"])){
        header("Location: ../sesion.html");
    }else{
        $res = ControlarFormularios::porId("empleado", $_GET["id"]);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bienvenido | PerDiem</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Página de bienvenida para administrador de PerDiem">
        <meta name="keywords" content="viaticos, administracion">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/3fe0600fc7.js" crossorigin="anonymous"></script>
       <!-- jQuery library -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Popper JS -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="../JavaScript/validaciones.js"></script>
        <link rel="stylesheet" type="text/css" href="../CSS/registro.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <a class="navbar-brand" href="#">PER DIEM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav nav-justified py-2 nav-pills">
                    <li class="nav-item">
                        <a href="autos.php?id=<?php echo $_GET["id"] ?>" class="nav-link">Administrar autos</a> <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="empleados.php?id=<?php echo $_GET["id"] ?>" class="nav-link" >Administrar empleados</a>
                    </li>
                    <li class="nav-item">
                        <a href="casetas.php?id=<?php echo $_GET["id"] ?>" class="nav-link">Administrar casetas</a>
                    </li>
                    <li class="nav-item">
                        <a href="../sesion.html" class="nav-link">Salir</a>
                    </li>
                </ul>
            </div>
          </nav>
    </body>
</html>
